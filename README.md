# Really Simple Google Tag

Really Simple Google Tag is a tiny module which allows site administrators to quickly and reliably add one or more Google Tag Manager tags to their websites.


## Requirements

None :)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## How to set this module up

Go to /admin/config/system/rsgt to configure this module.

1. Provide one or more Google Tag Manager tag IDs (in the format GTM-XXXXX), one per line in a textarea
2. Choose whether to include the tag on paths beginning /admin (default: no)
3. Choose whether to include the tag on paths beginning /user (default: yes)

Oh, and:

4: Click save :)

That's it!


## Help! It doesn't work!

Please report any issues in the
[project's issue queue](https://www.drupal.org/project/issues/really_simple_google_tag).

For a full description of the module, please visit the
[project homepage](https://www.drupal.org/project/really_simple_google_tag).


## Maintainers

- Alex Harries - [send spam and complaints here](https://www.drupal.org/u/alexharries).

:o)
