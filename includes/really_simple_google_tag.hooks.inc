<?php

/**
 * @file
 * Hook implementations for the really_simple_google_tag.module.
 */

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;

/**
 * Implements hook_help().
 */
function really_simple_google_tag_help($route_name): ?string {
  switch ($route_name) {
    // Main module help for the really_simple_google_tag module.
    case 'help.page.really_simple_google_tag':
      $output = '';
      $output .= '<h3>' . t("What's this then? :)") . '</h3>';
      $output .= '<p>' . t('Really Simple Google Tag is a tiny module which allows site administrators to quickly and reliably add one or more Google Tag Manager tags to their websites.') . '</p>';
      return $output;

    default:
      return NULL;
  }
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function really_simple_google_tag_menu_local_tasks_alter(&$data, $route_name, \Drupal\Core\Cache\RefinableCacheableDependencyInterface &$cacheability): void {
  // If we've been asked not to load tags for particular user roles, we need
  // to add a cache context to each page for the roles specified.
  if ($config = really_simple_google_tag_get_configuration()) {
    $excluded_roles = really_simple_google_tag_get_excluded_roles($config);

    foreach ($excluded_roles as $excluded_role) {
      $cacheability->addCacheContexts(['user.roles:' . $excluded_role]);
    }
  }
}

/**
 * Implements hook_page_attachments().
 */
function really_simple_google_tag_page_attachments(array &$attachments): void {
  if ($config = really_simple_google_tag_get_configuration()) {
    // Check if we should load the tag on this page.
    if (!really_simple_google_tag_load_on_this_page($config)) {
      return;
    }

    if (empty($attachments['#attached']['html_head'])) {
      $attachments['#attached']['html_head'] = [];
    }

    foreach (really_simple_google_tag_get_tag_ids($config) as $tag_id) {
      $attachments['#attached']['html_head'][] = [
        [
          '#type' => 'html_tag',
          '#tag' => 'script',
          '#value' => really_simple_google_tag_get_tag_script_string($tag_id),
        ],
        'really_simple_google_tag_' . $tag_id,
        '#weight' => 50,
      ];
    }
  }
}

/**
 * Implements hook_page_top().
 */
function really_simple_google_tag_page_top(array &$page): void {
  if ($config = really_simple_google_tag_get_configuration()) {
    // Check if we should load the tag on this page.
    if (!really_simple_google_tag_load_on_this_page($config)) {
      return;
    }

    foreach (really_simple_google_tag_get_tag_ids($config) as $tag_id) {
      $page += [
        'really_simple_google_tag_' . $tag_id => [
          '#type' => 'inline_template',
          '#template' => really_simple_google_tag_get_tag_noscript_string($tag_id),
          '#weight' => -50,
        ],
      ];
    }
  }
}

/**
 * Implements hook_really_simple_google_tag_noscript_string_alter().
 */
function really_simple_google_tag_really_simple_google_tag_noscript_string_alter(&$noscript_string): void {
  // Add some text which shows which module has inserted this tag.
  $start_text = t('Start Really Simple Google Tag');
  $end_text = t('End Really Simple Google Tag');

  $noscript_string = '<!-- ' . $start_text . ' -->'
    . PHP_EOL . $noscript_string . PHP_EOL
    . '<!-- ' . $end_text . ' -->' . PHP_EOL;
}

/**
 * Implements hook_really_simple_google_tag_script_string_alter().
 */
function really_simple_google_tag_really_simple_google_tag_script_string_alter(&$script_string): void {
  // Add some text which shows which module has inserted this tag.
  $start_text = t('Start Really Simple Google Tag');
  $end_text = t('End Really Simple Google Tag');

  $script_string = '// ' . $start_text
    . PHP_EOL . $script_string . PHP_EOL
    . '// ' . $end_text . PHP_EOL;
}
