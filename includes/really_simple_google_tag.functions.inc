<?php

/**
 * @file
 * Functionality for the really_simple_google_tag.module.
 */

use Drupal\really_simple_google_tag\ReallySimpleGoogleTagInterface;
use Drupal\user\Entity\User;

/**
 * Get the noscript HTML for a Google Tag.
 *
 * @param string $tag_id
 *   The Google Tag ID.
 *
 * @return string
 *   An HTML string, usually containing a <noscript /> element.
 */
function really_simple_google_tag_get_tag_noscript_string(string $tag_id): string {
  $noscript_string = '';

  // Create the noscript string, if the tag ID isn't empty.
  if (!empty($tag_id)) {
    $noscript_string = str_replace(ReallySimpleGoogleTagInterface::REPLACE_STRING, $tag_id, _really_simple_google_tag_get_tag_noscript_string_default());
  }

  return $noscript_string;
}

/**
 * Get the default noscript string.
 *
 * Other modules can implement
 * hook_really_simple_google_tag_noscript_string_alter to adjust this.
 *
 * @return string
 *   An HTML string, usually containing a <noscript /> element.
 */
function _really_simple_google_tag_get_tag_noscript_string_default(): string {
  $noscript_string = <<<MONKEY
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=%s"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
MONKEY;

  $noscript_string = sprintf($noscript_string, ReallySimpleGoogleTagInterface::REPLACE_STRING);

  // Implement a hook_really_simple_google_tag_noscript_string_alter.
  Drupal::moduleHandler()
    ->alter('really_simple_google_tag_noscript_string', $noscript_string);

  return $noscript_string;
}

/**
 * Get the script HTML for a Google Tag.
 *
 * @param string $tag_id
 *   The Google Tag ID.
 *
 * @return string
 *   A string of Javascript which loads the Google Tag for JS-enabled clients.
 */
function really_simple_google_tag_get_tag_script_string(string $tag_id): string {
  $script_string = '';

  // Create the script string, if the tag ID isn't empty.
  if (!empty($tag_id)) {
    $script_string = str_replace(ReallySimpleGoogleTagInterface::REPLACE_STRING, $tag_id, _really_simple_google_tag_get_tag_script_string_default());
  }

  return $script_string;
}

/**
 * Get the default script string.
 *
 * Other modules can implement
 * hook_really_simple_google_tag_script_string_alter to adjust this.
 *
 * @return string
 *   The default string of Javascript which loads the Google Tag for JS-enabled
 *   clients.
 */
function _really_simple_google_tag_get_tag_script_string_default(): string {
  $script_string = <<<MONKEY
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','%s');
MONKEY;

  $script_string = sprintf($script_string, ReallySimpleGoogleTagInterface::REPLACE_STRING);

  // Implement a hook_really_simple_google_tag_script_string_alter.
  Drupal::moduleHandler()
    ->alter('really_simple_google_tag_script_string', $script_string);

  return $script_string;
}

/**
 * Load the configuration for RSGT.
 *
 * @return \Drupal\Core\Config\ImmutableConfig|null
 *   A configuration object, if available, or null.
 */
function really_simple_google_tag_get_configuration(): ?\Drupal\Core\Config\ImmutableConfig {
  return Drupal::config(ReallySimpleGoogleTagInterface::CONFIG_NAME);
}

/**
 * Get the list of Google Tag IDs configured for the site (if any).
 *
 * @param \Drupal\Core\Config\ImmutableConfig $config
 *   The configuration object for this module.
 *
 * @return array
 *   An indexed array of tag IDs, or an empty array if none set.
 */
function really_simple_google_tag_get_tag_ids($config): array {
  return _really_simple_google_tag_configuration_textarea_to_array($config->get(ReallySimpleGoogleTagInterface::CONFIG_NAME_TAG_IDS));
}

/**
 * Get the list of Google Tag IDs configured for the site (if any).
 *
 * @param \Drupal\Core\Config\ImmutableConfig $config
 *   The configuration object for this module.
 *
 * @return array
 *   An indexed array of tag IDs, or an empty array if none set.
 */
function really_simple_google_tag_get_excluded_roles($config): array {
  return _really_simple_google_tag_configuration_textarea_to_array($config->get(ReallySimpleGoogleTagInterface::CONFIG_NAME_EXCLUDE_ROLES));
}

/**
 * Convert a string from a textarea into an array.
 *
 * @param $textarea_string
 *   The string to be turned into an array.
 *
 * @param bool $trim
 *   Whether to trim whitespace from the string and any array values.
 *   Default: TRUE.
 *
 * @param bool $filter_special_characters
 *   Whether to filter out any characters which aren't A-Z, a-z, 0-9, hyphens -
 *   and underscores _.
 *   Default: TRUE.
 *
 * @return array
 *   An array of trimmed values, one per line, or an empty array.
 */
function _really_simple_google_tag_configuration_textarea_to_array($textarea_string, bool $trim = TRUE, bool $filter_special_characters = TRUE): array {
  // Cast $textarea_string to a string.
  $textarea_string = (string) $textarea_string;

  if ($trim) {
    $textarea_string = trim($textarea_string);
  }

  $array = explode(PHP_EOL, $textarea_string);

  if (!empty($array)) {
    foreach ($array as $row => &$textarea_line) {
      // Are we trimming the string?
      if ($trim) {
        $textarea_line = trim($textarea_line);
      }

      // Are we stripping any speckle characters?
      if ($filter_special_characters) {
        // <heartemoji.gif> https://stackoverflow.com/a/6063236
        $textarea_line = preg_replace('/[^\w-]/', '', $textarea_line);
      }

      // Remove empty lines.
      if (empty($textarea_line)) {
        unset($array[$row]);
      }
    }
  }

  return $array;
}

/**
 * Check whether tags should be loaded on the current page for the current user.
 *
 * @param \Drupal\Core\Config\ImmutableConfig $config
 *   The configuration object for this module.
 *
 * @return bool
 *   TRUE if Google Tags can load on this page; FALSE if not.
 */
function really_simple_google_tag_load_on_this_page(\Drupal\Core\Config\ImmutableConfig $config): bool {
  // If we're not loading on /admin or /user paths, get the current path and
  // check.
  if (!$config->get(ReallySimpleGoogleTagInterface::CONFIG_NAME_ADD_ADMIN_PATHS)
    || !$config->get(ReallySimpleGoogleTagInterface::CONFIG_NAME_ADD_USER_PATHS)) {

    // Get the current path, and whether it is an admin route.
    $current_path_unaliased = \Drupal::service('path.current')->getPath();
    $current_path_aliased = strtolower(\Drupal::service('path_alias.manager')->getAliasByPath($current_path_unaliased));

    if (!$config->get(ReallySimpleGoogleTagInterface::CONFIG_NAME_ADD_ADMIN_PATHS)
      && (($current_path_aliased == '/admin') || str_starts_with($current_path_aliased, '/admin/'))) {
      return FALSE;
    }

    if (!$config->get(ReallySimpleGoogleTagInterface::CONFIG_NAME_ADD_USER_PATHS)
      && (($current_path_aliased == '/user') || str_starts_with($current_path_aliased, '/user/'))) {
      return FALSE;
    }
  }

  // Do we have any user roles which shouldn't see tags?
  $excluded_roles = really_simple_google_tag_get_excluded_roles($config);

  if (!empty($excluded_roles)) {
    // Get the user ID.
    $user_id = Drupal::currentUser()->id();

    // Loop through each excluded role. Only load the user object if the role
    // isn't "anonymous".
    foreach ($excluded_roles as $excluded_role) {
      // If the role isn't "anonymous" and the user ID > 0, load the user
      // object if we haven't already.
      if ($excluded_role == 'anonymous') {
        if ($user_id == 0) {
          return FALSE;
        }
      }
      else {
        // It must be an authenticated role. Validate the role exists.
        // (This might be a flawed assumption; can the anonymous user have
        // additional roles?)
        if (array_key_exists($excluded_role, user_role_names())) {
          if ($user_id > 0) {
            if (empty($user)) {
              // (We should check whether loading the user object like this
              // breaks cacheability.)
              $user = User::load($user_id);
            }

            if ($user->hasRole($excluded_role)) {
              return FALSE;
            }
          }
        }
      }
    }
  }

  // Default to returning TRUE if nothing has specifically said "no".
  return TRUE;
}
