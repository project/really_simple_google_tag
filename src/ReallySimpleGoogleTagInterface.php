<?php

namespace Drupal\really_simple_google_tag;

/**
 * Provides an interface for really_simple_google_tag constants.
 */
interface ReallySimpleGoogleTagInterface {

  /**
   * The configuration form's ID.
   */
  const CONFIG_FORM_ID = 'really_simple_google_tag_configuration_form';

  /**
   * The configuration object's machine name.
   */
  const CONFIG_NAME = 'really_simple_google_tag.configuration';

  /**
   * The tag ID configuration field name.
   */
  const CONFIG_NAME_TAG_IDS = 'google_tag_ids';

  /**
   * The text in the template strings to be replaced with the tag ID.
   */
  const REPLACE_STRING = 'TAG_ID';

  /**
   * The "add to /admin paths"? configuration field name.
   */
  const CONFIG_NAME_ADD_ADMIN_PATHS = 'add_to_admin_paths';

  /**
   * The "add to /user paths"? configuration field name.
   */
  const CONFIG_NAME_ADD_USER_PATHS = 'add_to_user_paths';

  /**
   * The excluded user roles configuration field.
   */
  const CONFIG_NAME_EXCLUDE_ROLES = 'exclude_roles';


}
