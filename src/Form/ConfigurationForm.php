<?php

namespace Drupal\really_simple_google_tag\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\really_simple_google_tag\ReallySimpleGoogleTagInterface;

/**
 * Provides the configuration form for RSGT.
 */
class ConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      ReallySimpleGoogleTagInterface::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return ReallySimpleGoogleTagInterface::CONFIG_FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(ReallySimpleGoogleTagInterface::CONFIG_NAME);

    // Get the original values.
    $original_config_values = $config->getOriginal('really_simple_google_tag');

    $form[ReallySimpleGoogleTagInterface::CONFIG_NAME_TAG_IDS] = [
      '#type' => 'textarea',
      '#title' => $this->t('Google Tag Identifiers, one per line'),
      '#description' => $this->t('Enter one Google Tag Manager ID per line - these will usually be in the format GTM-XXXXXXX. If you leave this field empty, this module won&#039;t add anything to your website.'),
      '#default_value' => $config->get(ReallySimpleGoogleTagInterface::CONFIG_NAME_TAG_IDS),
    ];

    $form[ReallySimpleGoogleTagInterface::CONFIG_NAME_ADD_ADMIN_PATHS] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add tags on paths which start with &quot;/admin&quot;?'),
      '#description' => $this->t('Do you want Google Tags to be added to paths which begin &quot;/admin&quot;? (Default: no)'),

      // Note use of check to see whether this value is FALSE or simply hasn't
      // been saved yet.
      '#default_value' => ($config->get(ReallySimpleGoogleTagInterface::CONFIG_NAME_ADD_ADMIN_PATHS) !== NULL)
        ? $config->get(ReallySimpleGoogleTagInterface::CONFIG_NAME_ADD_ADMIN_PATHS)
        : $original_config_values[ReallySimpleGoogleTagInterface::CONFIG_NAME_ADD_ADMIN_PATHS],
    ];

    $form[ReallySimpleGoogleTagInterface::CONFIG_NAME_ADD_USER_PATHS] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add tags on paths which start with &quot;/user&quot;?'),
      '#description' => $this->t('Do you want Google Tags to be added to paths which begin &quot;/user&quot;? (Default: yes)'),

      // Note use of check to see whether this value is FALSE or simply hasn't
      // been saved yet.
      '#default_value' => ($config->get(ReallySimpleGoogleTagInterface::CONFIG_NAME_ADD_USER_PATHS) !== NULL)
        ? $config->get(ReallySimpleGoogleTagInterface::CONFIG_NAME_ADD_USER_PATHS)
        : $original_config_values[ReallySimpleGoogleTagInterface::CONFIG_NAME_ADD_USER_PATHS],
    ];

    $form[ReallySimpleGoogleTagInterface::CONFIG_NAME_EXCLUDE_ROLES] = [
      '#type' => 'textarea',
      '#title' => $this->t('Don&#039;t load tags for users with the following roles'),
      '#description' => $this->t('If you don&#039;t want to load tags for certain groups of users, specify their roles&#039; <em>machine names</em> here, one per line. For example, you may wish to exclude administration users (e.g. <em>administrator</em>) and editors - for example if their visits would skew your analytics tracker&#039;s results. You can <a href="/admin/people/roles" target="_blank">get a list of your site\'s roles on the People > Roles page</a> (link opens in a new window).'),

      // Note use of check to see whether this value is FALSE or simply hasn't
      // been saved yet.
      '#default_value' => ($config->get(ReallySimpleGoogleTagInterface::CONFIG_NAME_EXCLUDE_ROLES) !== NULL)
        ? $config->get(ReallySimpleGoogleTagInterface::CONFIG_NAME_EXCLUDE_ROLES)
        : $original_config_values[ReallySimpleGoogleTagInterface::CONFIG_NAME_EXCLUDE_ROLES],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $this->config(ReallySimpleGoogleTagInterface::CONFIG_NAME)
      ->set(ReallySimpleGoogleTagInterface::CONFIG_NAME_TAG_IDS, $form_state->getValue(ReallySimpleGoogleTagInterface::CONFIG_NAME_TAG_IDS))
      ->set(ReallySimpleGoogleTagInterface::CONFIG_NAME_ADD_ADMIN_PATHS, $form_state->getValue(ReallySimpleGoogleTagInterface::CONFIG_NAME_ADD_ADMIN_PATHS))
      ->set(ReallySimpleGoogleTagInterface::CONFIG_NAME_ADD_USER_PATHS, $form_state->getValue(ReallySimpleGoogleTagInterface::CONFIG_NAME_ADD_USER_PATHS))
      ->set(ReallySimpleGoogleTagInterface::CONFIG_NAME_EXCLUDE_ROLES, $form_state->getValue(ReallySimpleGoogleTagInterface::CONFIG_NAME_EXCLUDE_ROLES))
      ->save();
  }

}
